# Manages an Enviroscan sensor
# from sentek

import modbus
import struct

class EnviroScan:
	address = None
	revision_code = None
	serial_number = None
	serial_port = None

	def __init__(self, address, serial_port = None):
		self.address = address
		self.address_str = "%02X" % self.address
		self.serial_port = serial_port
		
	def read_configuration(self):
		# Report slave ID command
		slave_id = modbus.send_command(self.serial_port, self.address_str + "11")			
		self.process_slave_id(slave_id)

	# Process a 'Report slave ID' command
	def process_slave_id(self, string):
		print "Processing Report Slave ID"
		if not check_packet_integrity(string):
			print "Bad string format, exiting"
			return
		byte_count_s = string[5:7]
		if int(byte_count_s, 16) != 20:
			print "Wrong number of bytes in Report Slave ID (%d != 14)" % int(byte_count_s, 16)
			return
		print "OK"


	def send_command(self, command, mode = "ASCII", 
					expected_answer_len = 1000,
					n_retries = 0):
		while n_retries >= 0:
			answer = modbus.send_command(self.serial_port, 
						   command, 
						   mode, 
						   expected_answer_len)
			if len(answer) > 0:
				return answer

			n_retries = n_retries - 1

		return ""

	# 01: Slave Address
	# 04: Read input registers
	# 00 01: Relative address (Add 30001 for abs)
	# 00 01: # Registers to read
	def read_input_registers(self, 
				rel_address, 
				n_registers = 1,
				n_retries = 0):
		if rel_address >= 30001:
			rel_address = rel_address - 30001
		command = ":%s04%s%s\r\n" % (self.address_str,
				  	     format_hex(rel_address, 2),
					     format_hex(n_registers, 2))
		tries = n_retries
		answer = self.send_command(command, n_retries = tries)
		return answer

	def request_slave_id(self):
		print "Requesting Slave ID"
		command = ":0111"
		self.send_command(command, n_retries = 5)

	def request_depth_info(self):
		print "Getting depth information"
		answer = self.read_input_registers(0x0064, n_registers = 4)
		self.process_depth_packet(answer)

	# Preset Holding Registers with value 2
	# (read values in broadcast). The moisture
	# values will be written in 32 bits starting
	# in register 30257. Each 32-bit floating
	# point value uses 2 consecutive 16-bit
	# register. The format is a mixture between
	# big endian and little endian:
	# Address 1 (Rel. 0) | Address 2 (Rel. 1)
	# Low word           | High word
	# ---------------------------------------
	# B. 15-8  | B. 7-0  | B. 31-24 | B. 23-16
	def start_measurements(self):
		print "Starting measurements in broadcast mode"
		command = ":000600000002"
		self.send_command(command)

	# Check reading progress: Read input register
	# 30002, query only 1 register.
	# Enviroscan reading process:
	# Details on page 17 of the Enviroscan
	# Modbus manual.
	# Answer:
	# 01 # Slave ID
	# 04 # Function
	# 02 # Byte count
	# 00 NN # Measurement status:
	#       # 0: No measurement
	#       # 1: Measurement in progress
	#       # 2: Measurement completed
	#       # 3: Measurement completed with errors
	# ?? # LRU
	def check_reading_progress(self):
		print "Checking reading progress"
		# command = ":010400010001"
		# self.send_command(command, n_retries = 5)
		answer = self.read_input_registers(30002, n_registers = 1)
		answer_arr = modbus.convert_to_array(answer)
		status = answer_arr[4]
		print "Measurement status:",
		if status == 0:
			print "No measurement"
		if status == 1:
			print "In progress"
		if status == 2:
			print "Measurement OK"
		if status == 3:
			print "Measurement with errors"
		return status		

	def process_depth_packet(self, string):
		arr = modbus.convert_to_array(string)
		n_sensors = len(arr) - 4
		depths = []
		sensor_id = 0
		print "Depths:"
		for ii in range(3, len(arr) - 1, 2):
			sensor_id = sensor_id + 1
			depth = ((arr[ii] << 8) + arr[ii + 1]) / 10.0
			print "  Sensor %d: %.1f" % (sensor_id, depth)
			depths = depths + [depth]
		return depths

	def process_cooked_data(self, string):
		if len(string) < 2:
			return None
		arr = modbus.convert_to_array(string)
		n_sensors = (len(arr) - 4) / 4
		values = []
		sensor_id = 0
		print "Values:"
		for ii in range(3, len(arr) - 1, 4):
			sensor_id = sensor_id + 1
			hex_str = ""
			hex_str = hex_str + ''.join(chr(i) for i in arr[(ii + 2):(ii + 4)])
			hex_str = hex_str + ''.join(chr(i) for i in arr[(ii):(ii + 2)])
			hex_str = hex_str[::-1]
			value = struct.unpack("<f", hex_str)[0]
			print "Sensor %d: %f" % (sensor_id, value)
			values = values + [value]
		
	# Read the measurements
	def read_measurements(self):
		print "Reading measurements"
		answer = self.read_input_registers(0x0100, n_registers = 8)
		self.process_cooked_data(answer)

# Check the integrity of the packet, verifying
# start btye and checksum
def check_packet_integrity(string):
	# Remove trailing spaces, or \r\n
	string = string.strip()
	if string[0] != ':':
		print "Bad format, should start with :"
		return False
	# Calculate LRU
	calc_lru = int(modbus.lru(string[0:(len(string) - 2)]), 16)
	# Compare with the last two chars
	packet_lru = int(string[(len(string) - 2):len(string)], 16)
	if calc_lru != packet_lru:
		print "Bad checksum, 0x%02X != 0x%02X" % (calc_lru, packet_lru)
		return False
	return True

# Transforms a number into a hex string
def format_hex(num, n_bytes = 1):
	if n_bytes == 1:
		return "%02X" % (num & 0xFF)
	return "%02X%02X" % ((num & 0xFF00) >> 8, num & 0xFF)

