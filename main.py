#!/usr/bin/env python
# Connects to a Sentek EnviroScan probe through
# the TTL port RS232 interface

import serial
import array
import os, sys, time
import enviroscan, modbus

def choose_serial_port():
	available_ports = os.popen("ls /dev/tty\.* | grep usbserial").read().strip().split()
	if len(available_ports) == 0:
		print "No serial ports available, exiting"
		sys.exit(0)

	port = available_ports[0]
	if len(available_ports) > 1:
		print "Available ports (%d):" % len(available_ports)
		print str(available_ports)
		print "Using %s" % port
	return port

measure_period = 10 # seconds
es = enviroscan.EnviroScan(address = 1)
tty_port = choose_serial_port()

# print modbus.crc([1, 2, 3, 4])
# s = serial.Serial('/dev/tty.usbserial-A601EFG9', 
s = serial.Serial(tty_port, 
                  baudrate = 9600, 
                  bytesize = serial.SEVENBITS,
                  parity   = serial.PARITY_NONE,
                  stopbits = serial.STOPBITS_TWO,
                  timeout  = 2)
modbus.safe_open(s)
es.serial_port = s

command = [0x01, 0x03, 0x00, 0x00, 0x00, 0x02]
command = [0x01, 0x11]

# Report Slave ID
es.request_slave_id()

# Restart Communications
# command = ":010800010000"
# send_command(s, command, mode="ASCII")

es.start_measurements()

while es.check_reading_progress() != 2:
	print "Waiting"
	time.sleep(1)

print "Making first measurements, should be invalid"
es.read_measurements()

print "Reading every %d seconds" % measure_period
while True:
	es.read_measurements()
	time.sleep(measure_period)
